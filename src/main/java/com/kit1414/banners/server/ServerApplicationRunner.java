package com.kit1414.banners.server;

import com.kit1414.banners.server.services.ServiceFactory;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ServerApplicationRunner
 * Main run class
 *
 *
 * Created by andrey.amelin on 21-Dec-15.
 */
public class ServerApplicationRunner {
    private static final Logger logger = Logger.getLogger(ServerApplicationRunner.class.getName());

    public static void main(String[] args) {
        String configFilePath = null;
        logger.log(Level.INFO, "args.length=" + args.length);
        if (args!= null) {
            for (int i = 0; i < args.length; i++) {
                logger.log(Level.INFO, "args["+i+"]=" + args[i]);
            }
        }
        int cfgIndex=0;
        if (args.length > cfgIndex) {
            configFilePath = args[cfgIndex];
            logger.log(Level.INFO, "config file path founded : configFilePath=" + configFilePath );
        } else {
            logger.log(Level.WARNING, "No config file path founded, use default values for config.");
        }

        ServiceFactory.initServices(configFilePath); // to init all static members
        ServiceFactory.startAll();

    }
}
