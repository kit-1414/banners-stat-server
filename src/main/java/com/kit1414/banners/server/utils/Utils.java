package com.kit1414.banners.server.utils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utils container
 *
 * Created by kit_1414 on 26-Dec-15.
 */
public class Utils {
    public static final String UTF_8_NAME = "utf-8";
    public static final Charset UTF_8_CHARSET = Charset.forName(UTF_8_NAME);
    public static final long MS_IN_MIN  = 60 * 1000;
    public static final long MS_IN_HOUR = 60 * MS_IN_MIN;

    private static final Logger logger = Logger.getLogger(Utils.class.getName());

    public static final String DEF_DATE_FORMAT = "dd/MM/yyyy  HH:mm:ss  SSSS";
    public static final SimpleDateFormat DEF_DATE_FORMATTER = new SimpleDateFormat(DEF_DATE_FORMAT);

    /**
     * replace in pattern substring like {0}, {1} etc by values with correspond number
     *
     * @param pattern  text with marks to replace
     *
     * @param values values to replace
     *
     * @return pattern text with replcaed values
     */


    public static String formatPatternByStrings(String pattern, Object ... values) {
        String result = pattern;
        if (values!= null && values.length > 0) {
            for(int i=0; i<values.length; i++) {
                String val=normObj(values[i]);
                String regexp = "\\{"+i+"\\}";
                result = result.replaceAll(regexp,val);
            }
        }
        return result;
    }

    /**
     *
     * replace in pattern substring like ${mapKey1}, ${mapKey1} etc by values from map
     *
     * @param pattern  text with marks to replace
     *
     * @param valuesMap map with keys (marks) and values to replace
     *
     * @return pattern text with replcaed values
     */
    public static String formatPatternByMap(String pattern, Map<String,Object> valuesMap) {
        String result = pattern;
        if (valuesMap!= null && valuesMap.size() > 0) {
            for(String key : valuesMap.keySet()) {
                String val=normObj(valuesMap.get(key));
                String regexp = "\\$\\{"+normObj(key)+"\\}";
                result = result.replaceAll(regexp,val);
            }
        }
        return result;
    }

    /**
     * return trimmed string, not null.
     *
     * @param inObj
     * @return
     */
    private static String normObj(Object inObj) {
        return inObj == null ? "null" : inObj.toString().trim();
    }

    /**
     * Returns file content as String
     *
     * @param file
     * @return file content
     */
    public static String loadFileAsText(File file) {
        String result = null;
        if (file.exists() && file.isFile() ) {
            Path path = Paths.get(file.getPath());
            try {
                result = new String(Files.readAllBytes(path), UTF_8_CHARSET);
            } catch (IOException ex) {
                logger.log(Level.INFO, "File read exception " + file.getAbsolutePath(), ex);
            }
        } else {
            logger.log(Level.INFO, "It's not a file or not exists " + file.getAbsolutePath());
        }
        return result;
    }

    /**
     * returns file content as String by file name in SRC resource folder.
     *
     * @param resourceName
     * @return
     */
    public static String loadResourceFileAsTextFromClasspath(String resourceName) {
        URL url= Utils.class.getClassLoader().getResource(resourceName);
        if (url!= null) {
            File file = new File(url.getFile());
            if (file!= null && file.exists() && file.isFile()) {
                return loadFileAsText(file);
            }
        }
        return null;
    }
    public static boolean deleteFile(Path path) {
        try {
            Files.delete(path);
            return true;
        } catch (NoSuchFileException x) {
            logger.log(Level.WARNING, "no such file or directory" + path);
        } catch (DirectoryNotEmptyException x) {
            logger.log(Level.WARNING, "directory not empty" + path);
        } catch (IOException x) {
            logger.log(Level.WARNING, "permission problem"+ path);
        }
        return false;
    }

    /**
     * Format date to String
     *
     * @param date
     * @return
     */

    public static String dateToString(Date date) {
        return date == null? "null" : DEF_DATE_FORMATTER.format(date);
    }

    /**
     * start timer
     *
     * @param task
     * @param interval
     * @return
     */
    public static Timer startTimer(TimerTask task, long interval) {
        Timer t = new Timer(true);
        t.scheduleAtFixedRate(task, interval, interval);
        return t;
    }

    public static void closeSocket(Closeable s) {
        if (s!= null) {
            try {
                s.close();
            } catch (Exception ex) {
                logger.log(Level.WARNING, "Socket close fail. " + ex, ex);
            }
        }
    }
    public static String getTrimmed(String string) {
        return string== null ? null : string.trim();
    }
    public static boolean hasText(String string) {
        return string!= null && string.trim().length() > 0;
    }

}
