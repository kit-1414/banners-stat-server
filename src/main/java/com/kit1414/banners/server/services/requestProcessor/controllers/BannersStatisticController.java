package com.kit1414.banners.server.services.requestProcessor.controllers;

import com.kit1414.banners.server.services.httpServer.HttpCode;
import com.kit1414.banners.server.services.httpServer.RequestData;
import com.kit1414.banners.server.services.httpServer.ResponseData;
import com.kit1414.banners.server.services.requestProcessor.IRequestController;
import com.kit1414.banners.server.services.statisticService.IStatisticsService;
import com.kit1414.banners.server.services.statisticService.TopBanner;
import com.kit1414.banners.server.services.statisticService.TopBanners;
import com.kit1414.banners.server.utils.Utils;
import com.kit1414.banners.server.services.statisticService.TopBannersMode;

import java.util.Date;
import java.util.HashMap;

/**
  * The BannersStatisticController provides action for top banners info requests.
  *
  * Created by kit_1414 on 26-Dec-15.
 */
public class BannersStatisticController extends ControllerBase implements IRequestController {
    private static final ResponseData INTERNAL_ERROR_RESPONSE = new ResponseData(HttpCode.ERR_500, null);

    public static final String STAT_BODY_PATTERN = "htmlPatterns/bannerStatResponseMain.html";
    public static final String STAT_ITEM_PATTERN = "htmlPatterns/bannerStatResponseItem.html";

    public static final String STAT_BODY = Utils.loadResourceFileAsTextFromClasspath(STAT_BODY_PATTERN);
    public static final String STAT_ITEM = Utils.loadResourceFileAsTextFromClasspath(STAT_ITEM_PATTERN);

    public static final String KEY_STAT_PAGE_TITLE   = "pageTitle";
    public static final String KEY_BANNERS_STAT_LIST = "bannersStatList";
    public static final String KEY_TIME_INFO = "timeInfo";



    private final IStatisticsService statisticsService;

    public BannersStatisticController(IStatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Override
    public ResponseData processRequest(RequestData request) {
        TopBannersMode e = getModeByUrl(request.getUri());
        if (e == null) {
            return INTERNAL_ERROR_RESPONSE;
        }
        TopBanners topStat = statisticsService.getBannersStatisticByMode(e);
        String html = getHtmlStatResponse(e.getPageTitle(), topStat);
        return new ResponseData (HttpCode.CODE_OK, html);
    }
    @Override
    public boolean isRequestMatch(RequestData rd) {
        return getModeByUrl(rd.getUri())!= null;
    }
    private static TopBannersMode getModeByUrl(String url) {
        url = url == null ? null : url.toLowerCase();
        if (url == null) {
            return null;
        }
        for (TopBannersMode mode : TopBannersMode.values()) {
            if (url.startsWith(mode.getUrl().toLowerCase())) {
                return mode;
            }
        }
        return null;
    }
    private static String getHtmlStatResponse(String pageTitle,TopBanners tsb) {
        HashMap<String, Object> map = new HashMap<>();
        TopBanner[] top100= new TopBanner[]{};
        String timerText = "";
        if (tsb!= null) {
            top100= tsb.getTopBanners();
            long lastRefreshTime = tsb.getStatCalTime();
            if (lastRefreshTime > 0) {
                String lastTimeUpdate = Utils.dateToString(new Date());
                String nextTimeUpdate = Utils.dateToString(new Date(tsb.getNextCalcTime()));
                long after = tsb.getNextCalcTime() - System.currentTimeMillis();

                timerText = "Stat updated " + lastTimeUpdate + ", next time update is " + nextTimeUpdate;
                if (after > 0) {
                    after = after / 1000;
                    timerText += ", after " + after + " seconds";
                }
            }
        }

        map.put(KEY_STAT_PAGE_TITLE,pageTitle);
        map.put(KEY_TIME_INFO,timerText);
        map.put(KEY_BANNERS_STAT_LIST,getBannersStatListHtml(top100).toString());
        String result = Utils.formatPatternByMap(STAT_BODY, map);
        return result;
    }
    private static String getBannersStatListHtml(TopBanner[] topBanners) {
        StringBuffer responseBuffer = new StringBuffer();
        int counter = 0;
        if (topBanners!= null) {
            for (int i=0; i< topBanners.length;i++){
                TopBanner bsb = topBanners[i];
                responseBuffer.append(Utils.formatPatternByStrings(STAT_ITEM,(++counter)+"",bsb.getBannerId(),bsb.getRequestNumber(), bsb.getLastRequestStr()));
            }
        }
        String result = responseBuffer.toString();
        return result;
    }
}
