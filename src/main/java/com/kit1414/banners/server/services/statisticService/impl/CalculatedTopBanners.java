package com.kit1414.banners.server.services.statisticService.impl;

import com.kit1414.banners.server.services.statisticService.TopBanner;
import com.kit1414.banners.server.services.statisticService.TopBannersMode;
import com.kit1414.banners.server.services.statisticService.TopBanners;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by kit_1414 on 03-Jan-16.
 *
 *  Bean to store calculated top banners info.
 *
 */

public class CalculatedTopBanners implements Serializable {
    /** bean creation time*/
    private final long creationTime;

    /** next refresh time*/
    private final long nextRefreshTime;

    /** top banners info by intervals*/
    private final Map<TopBannersMode, TopBanner[]> topStats;

    public CalculatedTopBanners(long creationTime, long nextRefreshTime) {
        this(creationTime,nextRefreshTime,new ConcurrentHashMap<TopBannersMode, TopBanner[]>());
    }
    public CalculatedTopBanners(long creationTime, long nextRefreshTime, Map<TopBannersMode, TopBanner[]> topStatsMap) {
        this.creationTime = creationTime;
        this.nextRefreshTime = nextRefreshTime;
        this.topStats = topStatsMap;
    }

    @Transient
    public TopBanners getTopStatResultBean(TopBannersMode e ) {
        return new TopBanners(creationTime, nextRefreshTime, topStats.get(e));
    }
}
