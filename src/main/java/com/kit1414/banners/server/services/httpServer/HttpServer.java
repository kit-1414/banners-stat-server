package com.kit1414.banners.server.services.httpServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ApplicationConfiguration;
import com.kit1414.banners.server.services.theradRunner.IThreadRunner;
import com.kit1414.banners.server.utils.Utils;

import sun.reflect.annotation.EnumConstantNotPresentExceptionProxy;

/**
 *
 * HTTP server implementation
 *
 * Created by andrey.amelin on 21-Dec-15.
 */

public class HttpServer extends Thread implements IHttpServer {
    private static final Logger logger = Logger.getLogger(HttpServer.class.getName());


    private ServerSocket serverSocket;
    private AtomicBoolean stopServerRequest = null;
    private int portNumber;
    private int maxConnNumber;
    private final IThreadRunner threadRunner;

    /** client threads pool*/
    private ExecutorService executorService;

    public HttpServer(ApplicationConfiguration cfg, IThreadRunner threadRunner) {
        this.portNumber = cfg .getServerPort();
        this.maxConnNumber = cfg.getMaxConnNumber();
        this.threadRunner = threadRunner;
    }

    /**
     * run sever
     */
    public void runServer() {
        final String fn = "runServer() ";
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(portNumber, maxConnNumber);
            logger.log(Level.WARNING, fn + "Server socket open on port: " + serverSocket.getLocalPort());
        } catch (IOException e) {
            Utils.closeSocket(serverSocket);
            serverSocket = null;
            logger.log(Level.WARNING, fn + "Port " + portNumber + " is blocked.");
        } catch (Exception ex) {
            logger.log(Level.WARNING, fn + "Unexpected error while server socket open on port " + portNumber);
            Utils.closeSocket(serverSocket);
            serverSocket = null;
        }

        if (serverSocket != null) {
            this.serverSocket = serverSocket;
            stopServerRequest = new AtomicBoolean(this.serverSocket.isClosed());
            this.start(); // server thread run
            logger.log(Level.WARNING, fn + "Server thread running...");
        }

        logger.log(Level.WARNING, fn + "<<<");
    }

    /**
     * Stop server main thread and all clients threads
     *
     * @return
     */
    public boolean stopServerRequest() {
        boolean result = false;
        final String fn = "Server close request()";
        logger.log(Level.INFO, fn + ">>>");
        if (stopServerRequest.compareAndSet(false,true)) {
            logger.log(Level.INFO, fn + "stopServerRequest set");
            try {
                if (serverSocket != null && !serverSocket.isClosed()) {
                    logger.log(Level.INFO, fn + "Server socket ready to close");
                    serverSocket.close();
                    this.interrupt(); //interrupt server thread
                    logger.log(Level.INFO, fn + "Server closed OK");
                    result = true;
                }
            } catch (IOException ex) {
                logger.log(Level.INFO, fn + "Problem to close server socket: ", ex);
            }
        }
        logger.log(Level.INFO, fn + " <<< result=" + result);
        return result;
    }

    /**
     *  Waiting for all clients threads to finish ro kill them all.
     *
     * @return
     */

    /**
     * Server thread to accept clients
     *
     */
    public void run() {
        logger.log(Level.INFO, "Server thread started >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        boolean acceptThreadRun = true;
        while (acceptThreadRun && !serverSocket.isClosed()) {
            try {
                Socket clientSocket = serverSocket.accept();
                clientSocket.setSoTimeout(1000); // socketTimeout in ms
                threadRunner.startThread(new ClientSession(clientSocket));
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
            } catch (SocketException ex) {
                logger.log(Level.INFO, "Server thread closed, stop application.");
                acceptThreadRun = false;
            } catch (InterruptedException ex) {
                logger.log(Level.INFO, "Server thread interrupted, stop application.");
                acceptThreadRun = false;
            } catch (IOException e) {
                acceptThreadRun = false;
                logger.log(Level.WARNING, "Failed to establish connection.", e);
            }
        }
        Utils.closeSocket(serverSocket);
        logger.log(Level.INFO, "Server thread finished");
    }

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public int getMaxConnNumber() {
        return maxConnNumber;
    }

    public void setMaxConnNumber(int maxConnNumber) {
        this.maxConnNumber = maxConnNumber;
    }
}