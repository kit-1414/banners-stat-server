package com.kit1414.banners.server.services.theradRunner.impl;

import com.kit1414.banners.server.services.theradRunner.IThreadRunner;

/**
 * Created by aamelin on 31.10.2016.
 *
 * The IThreadRunner service implementation by simple java Thread.
 */
public class ThreadRunnerImplSimple implements IThreadRunner {

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void startThread(Runnable runnable) {
        Thread t = new Thread(runnable);
        t.start();
    }
}
