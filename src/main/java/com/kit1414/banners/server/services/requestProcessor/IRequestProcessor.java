package com.kit1414.banners.server.services.requestProcessor;

import java.io.IOException;

import com.kit1414.banners.server.services.httpServer.ClientSession;
import com.kit1414.banners.server.services.httpServer.ResponseData;

/**
 *
 * The interface presents the API for HTTP requests processor.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public interface IRequestProcessor {
    ResponseData processRequest(ClientSession cs) throws IOException;
}
