package com.kit1414.banners.server.services.statisticService.impl;


import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ApplicationConfiguration;
import com.kit1414.banners.server.services.statisticService.IStatisticsService;
import com.kit1414.banners.server.services.statisticService.TopBanners;
import com.kit1414.banners.server.services.statisticService.TopBannersMode;
import com.kit1414.banners.server.utils.Utils;


/**
 *
 * The Banner Statistic service implementation
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class StatisticServiceImpl extends TimerTask implements IStatisticsService {
    private static final Logger logger = Logger.getLogger(StatisticServiceImpl.class.getName());

    /** StatisticManager class implements detailed logic of statistics management. */
    private final StatisticManager statManager = new StatisticManager();

    // timer interval to refresh top banners stat and clear expired info
    private long timerInterval;

    // the Timer to refresh top banners stat and clear expired info
    private Timer timer;

    /**
     * parameters initialization
     *
     * @param cfg
     */
    public void init(ApplicationConfiguration cfg) {
        timerInterval = cfg.getStatisticsServiceTimerIntervalInSeconds()*1000;
        statManager.init(cfg);
    }

    @Override
    public void processBannerRequest(String bannerId) {
        if (!statManager.processBannerRequest(bannerId,System.currentTimeMillis())) {
            logger.log(Level.WARNING, " Can't process request for banner " + bannerId + " due to queue is full.");
        }

    }
    @Override
    public void stopService() {
        statManager.stopService();
        timer.cancel();
        timer=null;
        logger.log(Level.INFO, "Service stopped.");
    }
    @Override
    public void startService() {
        this.timer = Utils.startTimer(this,timerInterval);
        statManager.startTasksWaitingThread();
        logger.log(Level.INFO, "Service started.");
    }

    @Override
    public TopBanners getBannersStatisticByMode(TopBannersMode e) {
        return statManager.getTopStatByMode(e);
    }
    @Override
    /**
     *
     * Timer task to refresh top banners stat and clear expired info
     */
    public void run() {
        try {
            logger.log(Level.INFO, " Statistic Service OnTime() >>> ");
            long nowTime = System.currentTimeMillis();
            statManager.recalculateTopStat(nowTime);
            logger.log(Level.INFO, " Statistic Service OnTime() <<< ");
        } catch (Exception ex) {
            logger.log(Level.WARNING, " Statistic Service OnTime() failed ",ex);
        }
    }
}

