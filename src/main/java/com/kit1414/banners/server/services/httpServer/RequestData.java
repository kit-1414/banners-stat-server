package com.kit1414.banners.server.services.httpServer;

import com.kit1414.banners.server.utils.Utils;

/**
 *
 * The class presents HTTP request data.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class RequestData {
    private String method = null;
    private String uri = null;
    private String params = null;
    private String protocol = null;
    private String qs = null;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getQs() {
        return qs;
    }

    public void setQs(String qs) {
        this.qs = qs;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public boolean isHeaderValid() {
        return Utils.hasText(method) && Utils.hasText(protocol);
    }

    @Override
    public String toString() {
        return "RequestData{" +
                "method='" + method + '\'' +
                ", uri='" + uri + '\'' +
                ", params='" + params + '\'' +
                ", qs='" + qs + '\'' +
                ", protocol='" + protocol + '\'' +

                '}';
    }
}

