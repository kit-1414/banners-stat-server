package com.kit1414.banners.server.services.bannersService;

/**
 * The interface provides the API for Banner service
 *
 * Created by andrey.amelin on 21-Dec-15.
 */
public interface IBannerService {

    /**
     * return banner body, if banner file exists and doesn't expired.
     * @param bannerName
     * @return
     */
    String getBannerBody(String bannerName);

    /**
     * start service
     */
    void startService();

    /**
     * stop service
     */
    void stopService();
}
