package com.kit1414.banners.server.services.requestProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.httpServer.ClientSession;
import com.kit1414.banners.server.services.httpServer.HttpCode;
import com.kit1414.banners.server.services.httpServer.RequestData;
import com.kit1414.banners.server.services.httpServer.ResponseData;
import com.kit1414.banners.server.utils.Utils;

/**
 * HTTP processor implementation
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class RequestProcessorImpl implements IRequestProcessor {
    private static final Logger logger = Logger.getLogger(RequestProcessorImpl.class.getName());
    private static final ResponseData FORBIDDEN_RESPONSE= new ResponseData(HttpCode.ERR_403, null);
    private static final ResponseData SERVER_ERROR_RESPONSE = new ResponseData(HttpCode.ERR_500, null);

    private List<IRequestController> controllers ;

    public List<IRequestController> getControllers() {
        return controllers;
    }

    public void setControllers(List<IRequestController> controllers) {
        this.controllers = controllers;
    }

    public void init (IRequestController ... controllersIn) {
        ArrayList<IRequestController> newControllers = new ArrayList<>();
        if (controllersIn!= null) {
            for (IRequestController controller : controllersIn) {
                if (controller!= null)
                    newControllers .add(controller);
            }
        }
        setControllers(newControllers);
    }

    @Override
    public ResponseData processRequest(ClientSession clientSession)  throws IOException{
        String header = clientSession.readHeader();
        logger.log(Level.INFO,"header=" + header);
        RequestData requestData = clientSession.parseHeader(header);
        logger.log(Level.INFO, requestData.toString());

        if (!requestData.isHeaderValid()) {
            logger.log(Level.INFO, "Invalid header :" + requestData);
            return SERVER_ERROR_RESPONSE;
        }

        if (controllers!= null) {
            for (IRequestController controller: controllers) {
                try {
                    if (controller.isRequestMatch(requestData)) {
                        logger.log(Level.INFO, "processor matched :" + controller.getClass().getName());
                        return controller.processRequest(requestData);
                    }
                } catch (Exception ex) {
                    logger.log(Level.WARNING, "Controller fails:" + controller.getClass().getName(),ex);
                    return SERVER_ERROR_RESPONSE;
                }
            }
        }
        return FORBIDDEN_RESPONSE;
    }

}
