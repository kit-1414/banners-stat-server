package com.kit1414.banners.server.services.requestProcessor.controllers;

import com.kit1414.banners.server.services.ServiceFactory;
import com.kit1414.banners.server.services.httpServer.HttpCode;
import com.kit1414.banners.server.services.httpServer.RequestData;
import com.kit1414.banners.server.services.httpServer.ResponseData;
import com.kit1414.banners.server.services.requestProcessor.IRequestController;
import com.kit1414.banners.server.utils.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The StopServerController provides action to stop application request.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class StopServerController extends ControllerBase implements IRequestController {
    private static final Logger logger = Logger.getLogger(StopServerController.class.getName());

    public static final String STOP_SRV_PATTERN = "htmlPatterns/stopServerResponse.html";
    public static final String STOP_SRV_RESP = Utils.loadResourceFileAsTextFromClasspath(STOP_SRV_PATTERN);

    private static final ResponseData HTTP_RESPONSE=new ResponseData(HttpCode.CODE_OK, STOP_SRV_RESP);

    private String URL_START = URL_STOP_SERVER;

    @Override
    public ResponseData processRequest(RequestData rd) {
        logger.log(Level.INFO,"processor >>>");
        ServiceFactory.stopAll();
        logger.log(Level.INFO,"processor <<<");
        return HTTP_RESPONSE;
    }
    @Override
    public boolean isRequestMatch(RequestData rd) {
        return URL_START.equalsIgnoreCase(rd.getUri());
    }
}
