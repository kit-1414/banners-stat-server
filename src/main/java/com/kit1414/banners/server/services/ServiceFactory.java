package com.kit1414.banners.server.services;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.bannersService.BannerServiceImpl;
import com.kit1414.banners.server.services.bannersService.IBannerService;
import com.kit1414.banners.server.services.httpServer.HttpServer;
import com.kit1414.banners.server.services.httpServer.IHttpServer;
import com.kit1414.banners.server.services.requestProcessor.IRequestController;
import com.kit1414.banners.server.services.requestProcessor.IRequestProcessor;
import com.kit1414.banners.server.services.requestProcessor.RequestProcessorImpl;
import com.kit1414.banners.server.services.requestProcessor.controllers.BannersStatisticController;
import com.kit1414.banners.server.services.requestProcessor.controllers.GetBannerController;
import com.kit1414.banners.server.services.requestProcessor.controllers.StopServerController;
import com.kit1414.banners.server.services.statisticService.IStatisticsService;
import com.kit1414.banners.server.services.statisticService.impl.StatisticServiceImpl;
import com.kit1414.banners.server.services.theradRunner.IThreadRunner;
import com.kit1414.banners.server.services.theradRunner.impl.ThreadRunnerImplExecutor;

/**
 * The class provides services access/creation/start/stop functionality
 *
 * Created by andrey.amelin on 21-Dec-15.
 */
public class ServiceFactory {

    private static final Logger logger = Logger.getLogger(ServiceFactory.class.getName());

    private static ApplicationConfiguration config = null;

    private static IHttpServer httpServer = null;
    private static IRequestProcessor requestProcessor;
    private static IStatisticsService statisticsService;
    private static IBannerService bannerService;
    private static IThreadRunner threadRunner;
    private static AtomicBoolean systemRun = new AtomicBoolean(false);

    /**
     * The method creates all services and init all their fields.
     *
     * @param pathToConfigFile
     */
    public static void initServices(String pathToConfigFile)
    {
        final String fn = "initServices() ";
        logger.log(Level.INFO, fn  + " >>> pathToConfigFile=" + pathToConfigFile);

        config = initConfigFile(pathToConfigFile);

        threadRunner = initThreadRunner(config);
        httpServer = initHttpServer(config, threadRunner);
        bannerService = initBannerService(config);

        statisticsService = initStatisticsService(config);

        requestProcessor = initRequestProcessor(config, httpServer, bannerService, statisticsService);

        logger.log(Level.INFO, fn  + " config="+config);
        logger.log(Level.INFO, fn  + " <<<");

    }

    /**
     * The method init config instance from properties file.
     *
     * @param pathToConfigFile
     * @return
     */
    private static ApplicationConfiguration initConfigFile(String pathToConfigFile) {
        final String fn="initConfigFile() ";
        ApplicationConfiguration configLocal = new ApplicationConfiguration();
        File file = null;
        if (pathToConfigFile!= null && pathToConfigFile.length() > 0) {
            file = new File(pathToConfigFile);
        }
        Properties prop = null;
        if (file!= null && file.exists() && file.isFile()) {
            logger.log(Level.INFO, fn + " properties file ok, read it");
            try ( FileInputStream fis = new FileInputStream(pathToConfigFile)) {
                prop = new Properties();
                prop.load(fis);
                logger.log(Level.INFO, fn + " properties file loaded ok");
            } catch (Exception ex) {
                logger.log(Level.WARNING, fn + " properties file load fails", ex);
                prop = null;
            }
            if (prop!= null) {
                logger.log(Level.INFO, fn + " configuration loading started");
                configLocal.loadConfig(prop);
                logger.log(Level.INFO, fn + " configuration loaded ok");

            }
        }
        return configLocal;
    }

    /**
     * The method stop all services, which must be stopped before app finished.
     *
     */
    public static void stopAll() {
        if (systemRun.compareAndSet(true,false)) {
            httpServer.stopServerRequest();
            bannerService.stopService();
            statisticsService.stopService();
            threadRunner.stop();
        }
    }
    /**
     * The method start all services, which must be started to work app OK.
     *
     */
    public static void startAll() {
        if (systemRun.compareAndSet(false,true)) {
            threadRunner.start();
            statisticsService.startService();
            bannerService.startService();
            httpServer.runServer();
        }
    }

    /**
     * The method creates Thread runner service.
     *
     * @param cfg
     * @return
     */
    private static IThreadRunner initThreadRunner(ApplicationConfiguration cfg) {
        IThreadRunner threadRunner = new ThreadRunnerImplExecutor(cfg);
        //IThreadRunner threadRunner = new ThreadRunnerImplSimple(cfg);
        return threadRunner;
    }

    /**
     * The method creates HTTP Server service and init their fields.
     *
     * @param cfg
     * @return
     */
    private static IHttpServer initHttpServer(ApplicationConfiguration cfg, IThreadRunner threadRunner) {
        HttpServer httpServerLocal = new HttpServer(cfg,threadRunner);
        return httpServerLocal;
    }


    /**
     * The method creates HTTP Request processor service and init their fields.
     *
     * @param cfg
     * @param httpServer
     * @param bannerService
     * @param statisticsService
     * @return
     */
    private static IRequestProcessor initRequestProcessor(ApplicationConfiguration cfg, IHttpServer httpServer, IBannerService bannerService, IStatisticsService statisticsService) {
        RequestProcessorImpl requestProcessorLocal = new RequestProcessorImpl();
        requestProcessorLocal.init(new IRequestController[]{
                        new GetBannerController(bannerService, statisticsService),
                        new StopServerController(),
                        new BannersStatisticController(statisticsService)
                }
        ) ;
        return requestProcessorLocal;
    }

    /**
     *
     * The method creates Statistics service and init their fields.
     *
     * @param cfg
     * @return
     */
    private static IStatisticsService initStatisticsService(ApplicationConfiguration cfg) {
        StatisticServiceImpl statisticService = new StatisticServiceImpl();
        statisticService.init(cfg);
        return statisticService;
    }

    /**
     * The method creates Banner access service and init their fields.
     *
     * @param cfg
     * @return
     */
    private static IBannerService initBannerService(ApplicationConfiguration cfg) {
        BannerServiceImpl bannerServiceLocal = new BannerServiceImpl();
        bannerServiceLocal .init(cfg);
        return bannerServiceLocal ;
    }

    // getters
    public static ApplicationConfiguration getApplicationConfiguration() {
        return config;
    }
    public static IStatisticsService getStatisticsService() {
        return statisticsService;
    }

    public static IHttpServer getHttpServer() {
        return httpServer;
    }

    public static IRequestProcessor getRequestProcessor() {
        return requestProcessor;
    }
}


