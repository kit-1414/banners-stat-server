package com.kit1414.banners.server.services.statisticService;

import com.kit1414.banners.server.utils.Utils;
import com.kit1414.banners.server.services.requestProcessor.controllers.IUrlMapping;

/**
 * The enum presents time intervals to store top banners statistic.
 *
 * Created by kit_1414 on 26-Dec-15.
 */
public enum TopBannersMode {

    TOP_100_24_H(IUrlMapping.URL_STAT_TOP_24_H,  "Top 100 banners for 24 hours",  Utils.MS_IN_HOUR * 24)
    ,TOP_100_4_H(IUrlMapping.URL_STAT_TOP_4_H,  "Top 100 banners for 4 hours",    Utils.MS_IN_HOUR * 4)
    ,TOP_100_1_H(IUrlMapping.URL_STAT_TOP_1_H,  "Top 100 banners for 1 hour",     Utils.MS_IN_HOUR * 1)
    ,TOP_100_1_MIN(IUrlMapping.URL_STAT_TOP_1_MIN,"Top 100 banners for 1 min",    Utils.MS_IN_MIN)
    ;

    /** The Http URL path pattern to request top banners stat for the interval */
    private final String url;

    /** the HTML response page title for  top stat request*/
    private final String pageTitle;

    /** time interval for requested statistic in ms */
    private final long timeInterval;

    TopBannersMode(String url, String pageTitle, long timeInterval) {
        this.url = url;
        this.pageTitle = pageTitle;
        this.timeInterval = timeInterval;
    }

    public String getUrl() {
        return url;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public long getTimeInterval() {
        return timeInterval;
    }

    @Override
    public String toString() {
        return "TopBannersMode{" +
                "url='" + url + '\'' +
                ", pageTitle='" + pageTitle + '\'' +
                ", timeInterval=" + timeInterval +
                '}';
    }
}
