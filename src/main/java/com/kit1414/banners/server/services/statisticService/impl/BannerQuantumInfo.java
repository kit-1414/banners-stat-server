package com.kit1414.banners.server.services.statisticService.impl;

import java.beans.Transient;
import java.io.Serializable;

/**
 *
 * Class stores banner requests number for time interval
 *
 * Created by kit_1414 on 27-Dec-15.
 *
 */
public class BannerQuantumInfo implements Serializable {
    /** quantum start time in ms*/
    private final long startMomentMs;

    /** quantum time interval in ms */
    private final long interval;

    /** last request time for the quantum */
    private long lastRequestTime;

    /** banner requests number for the interval */
    private long requestsNumber;

    public BannerQuantumInfo(long startMomentMs, long interval) {
        this.startMomentMs = startMomentMs;
        this.interval = interval;
        this.lastRequestTime = startMomentMs;
        this.requestsNumber = 0;
    }

    public long getRequestsNumber() {
            return requestsNumber;
    }

    public long addRequestsNumber(long requestsToAdd, long lastRequestTime) {
        this.requestsNumber += requestsToAdd;
        this.lastRequestTime =lastRequestTime;
        return getRequestsNumber();
    }

    public long getLastRequestTime() {
        return lastRequestTime;
    }

    /**
     * check does time moment inside quantum
     *
     * @param timeMomentInMs - time moment to check in ms.
     *
     * @return true, if time inside quantum. Else another case.
     */
    @Transient
    public boolean isInsideQuantum(long timeMomentInMs) {
        return timeMomentInMs >= startMomentMs && timeMomentInMs <=startMomentMs + interval;
    }
    @Transient
    public boolean isBeforeQuantum(long timeMomentInMs) {
        return startMomentMs > timeMomentInMs;
    }
    @Transient
    public boolean isAfterQuantum(long timeMomentInMs) {
        return startMomentMs + interval < timeMomentInMs;
    }

    @Transient
    public boolean isIntersect(long startTime ,long interval) {
        long endTime= startTime + interval;
        return isInsideQuantum(startTime) || isInsideQuantum(endTime) ||
                (isBeforeQuantum(startTime) && isAfterQuantum(endTime)); // interval contains quantum
    }

    @Transient
    public boolean isExpiredForTime(long timeMomentInMs) {
        return isAfterQuantum(timeMomentInMs);
    }

    @Override
    public String toString() {
        return "BannerQuantumInfo{" +
                "startMomentMs=" + startMomentMs +
                ", interval=" + interval +
                ", lastRequestTime=" + lastRequestTime +
                ", requestsNumber=" + requestsNumber +
                '}';
    }
}
