package com.kit1414.banners.server.services.bannersService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ApplicationConfiguration;
import com.kit1414.banners.server.services.IServiceConstants;
import com.kit1414.banners.server.utils.Utils;

/**
 * The Banner service implementation.
 * <p>
 * Service provides features :
 * - it return banner file content from file system, if file exists and doesn't expired
 * - clean expired banner files by timer task
 * <p>
 * Created by andrey.amelin on 21-Dec-15.
 */
public class BannerServiceImpl extends TimerTask implements IBannerService {
    private static final Logger logger = Logger.getLogger(BannerServiceImpl.class.getName());
    private String bannerRootFolder;
    private long expirationIntervalInMs;
    private long timerIntervalInMs;
    private static final String BANNER_FILE_END = ".html";
    private Timer timer;

    public void init(ApplicationConfiguration cfg) {
        bannerRootFolder = cfg.getRootBannersFolder();
        expirationIntervalInMs = IServiceConstants.BANNER_FILE_EXPIRATION_INTERVAL_HRS * Utils.MS_IN_HOUR;
        timerIntervalInMs = cfg.getBannersServiceTimerIntervalInSeconds() * 1000;
    }

    public String getBannerBody(String bannerName) {
        String fullFileName = getFullBannerName(bannerName);
        File file = new File(fullFileName);

        logger.log(Level.INFO, "requested banner id=" + bannerName);
        logger.log(Level.INFO, "requested banner fullName=" + fullFileName);
        logger.log(Level.INFO, "current path=" + getAbsolutePath());

        String bannerBody = null;

        // TODO fle content && creation time can be cached.

        // NPE

        if (file.exists() && file.isFile() && !isExpired(fullFileName)) {
            bannerBody = Utils.loadFileAsText(file);
        } else {
            logger.log(Level.INFO, "banner is not a file or not exists " + fullFileName);
        }
        return bannerBody;
    }

    public String getFullBannerName(String bannerId) {
        return bannerRootFolder + "/" + bannerId + BANNER_FILE_END;
    }

    public String getAbsolutePath() {
        Path currentRelativePath = Paths.get("");
        return currentRelativePath.toAbsolutePath().toString();
    }

    private boolean isExpired(String path) {
        // path must be valid file path.
        Path file = Paths.get(path);
        try {
            BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
            FileTime ft = attr.creationTime();
            long fileMs = ft.toMillis();
            long curTime = System.currentTimeMillis();
            return fileMs + expirationIntervalInMs < curTime;
        } catch (IOException ex) {
            String errMsg = "Can't read attributes for file [" + path + "]";
            logger.log(Level.INFO, errMsg);
            throw new IllegalStateException(errMsg, ex);
        }
    }

    /**
     * timer task to delete expired files from file system.
     */
    @Override
    public void run() {
        try {
            logger.log(Level.INFO, " banner Service OnTime() >>> ");
            removeExpiredFiles(this.bannerRootFolder);
            logger.log(Level.INFO, " banner Service OnTime() <<< ");
        } catch (Exception ex) {
            logger.log(Level.WARNING, " banner Service OnTime() failed ", ex);
        }
    }

    public void removeExpiredFiles(String path) {

        File root = new File(path);
        File[] list = root.listFiles();

        if (list == null) {
            return;
        }

        for (File f : list) {
            if (f.isDirectory()) {
                removeExpiredFiles(f.getAbsolutePath());
            } else {
                String fileFullPath = f.getAbsoluteFile().getPath();
                logger.log(Level.INFO, "on time check file : " + fileFullPath);
                if (fileFullPath.toLowerCase().endsWith(BANNER_FILE_END) && isExpired(fileFullPath)) {
                    logger.log(Level.INFO, "banner file expired : " + fileFullPath);
                    if (Utils.deleteFile(Paths.get(fileFullPath))) {
                        logger.log(Level.WARNING, "can't delete expired file " + fileFullPath);
                    } else {
                        logger.log(Level.INFO, "expired banner file removed ok " + fileFullPath);
                    }
                }
            }
        }
    }

    @Override
    public void startService() {
        timer = Utils.startTimer(this, timerIntervalInMs);
    }

    @Override
    public void stopService() {
        timer.cancel();
        timer = null;
    }
}
