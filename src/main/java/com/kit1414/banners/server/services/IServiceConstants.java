package com.kit1414.banners.server.services;

/**
 *
 * The interface contains some constants for the application.
 *
 * Created by kit_1414 on 27-Dec-15.
 *
 */

public interface IServiceConstants {
    /**  top banners number to show statistics for intervals. */
    int topBannerNumber = 100;

    /** banner file expiration interval in hrs*/
    int BANNER_FILE_EXPIRATION_INTERVAL_HRS = 24 * 14 ; // 2 weeks ago
}

