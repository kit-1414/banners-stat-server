package com.kit1414.banners.server.services.requestProcessor;

import com.kit1414.banners.server.services.httpServer.RequestData;
import com.kit1414.banners.server.services.httpServer.ResponseData;

/**
 * Interface presents controller API to process HTTP requests
 *
 * Created by kit_1414 on 26-Dec-15.
 */
public interface IRequestController {
    /**
     * @param rd
     * @return true, if URL path matches the requests and The controller must process the request.
     */
    boolean isRequestMatch(RequestData rd);


    /**
     * process HTTP request
     * 
     * @param request
     * @return
     */
    ResponseData processRequest(RequestData request);
}
