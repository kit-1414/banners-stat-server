package com.kit1414.banners.server.services.httpServer;

/**
 * The class presents HTTP response data.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class ResponseData {
    private HttpCode code;
    private String responseHtml;

    public ResponseData(HttpCode code, String responseHtml) {
        this.code = code;
        this.responseHtml = responseHtml;
    }

    public ResponseData() {
    }

    public HttpCode getCode() {
        return code;
    }

    public void setCode(HttpCode code) {
        this.code = code;
    }

    public String getResponseHtml() {
        return responseHtml;
    }

    public void setResponseHtml(String responseHtml) {
        this.responseHtml = responseHtml;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "code=" + code +
                ", responseHtml='" + responseHtml + '\'' +
                '}';
    }
}
