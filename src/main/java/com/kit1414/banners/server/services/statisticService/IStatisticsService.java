package com.kit1414.banners.server.services.statisticService;

/**
 * The Banner Statistic service API : service stores statistics of OK banner requests for enumerated time intervals.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public interface IStatisticsService {
    /**
     *
     * @param mode
     *
     * @return top banners statistics for requested time inter
     */
    TopBanners getBannersStatisticByMode(TopBannersMode mode);

    /**
     * notify service about OK banner request
     * @param bannerId
     */
    void processBannerRequest(String bannerId);

    /**
     * start service
     */
    void stopService();

    /**
     * stop service
     */
    void startService();
}
