package com.kit1414.banners.server.services.httpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ServiceFactory;
import com.kit1414.banners.server.services.requestProcessor.IRequestProcessor;
import com.kit1414.banners.server.utils.Utils;


/**
 * Class presents Http clients socket logic.
 *
 * Created by andrey.amelin on 21-Dec-15.
 */

public class ClientSession implements Runnable {
    public static final String LS = System.getProperty("line.separator");
    private static final Logger logger = Logger.getLogger(ClientSession.class.getName());

    private final Socket socket;
    private InputStream in = null;
    private OutputStream out = null;

    @Override
    /**
     * main client thread
     */
    public void run() {
        if (socket!= null && !socket.isClosed()) {
            logger.log(Level.INFO, " Client session started >>>" );

            try {
                IRequestProcessor requestProcessor = ServiceFactory.getRequestProcessor();
                ResponseData responseData = requestProcessor.processRequest(this);
                logger.log(Level.INFO," Result code="+responseData .getCode());
                send(responseData.getCode(), responseData.getResponseHtml());
            } catch (Exception ex) {
                logger.log(Level.WARNING,"Client thread failed ",ex);
                send(HttpCode.ERR_500, "");
            } finally {
                Utils.closeSocket(socket);
            }
            logger.log(Level.INFO,"Client session Finished, socket closed : "  + socket.isClosed());
        }
    }

    public ClientSession(Socket socket) throws IOException {
        this.socket = socket;
        initialize();
    }

    private void initialize() throws IOException {
        in = socket.getInputStream();
        out = socket.getOutputStream();
    }

    public String readHeader() throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder builder = new StringBuilder();
            String ln = null;
            while (true) {
                ln = reader.readLine();
                if (ln == null || ln.isEmpty()) {
                    break;
                }
                builder.append(ln + LS);
            }
            return builder.toString();
        } catch (SocketTimeoutException ex) {
            // all data interaction must be fast, else some wrong
            logger.log(Level.INFO,"Client session read timeout happen, socket closed : "  + socket.isClosed());
            return null;
        }
    }

    /**
     * create request data class instance
     *
     * @param header
     * @return
     */
    public RequestData parseHeader(String header) {
        RequestData rd = new RequestData();
        String methodLine = null;
        if (hasText(header)) {
            String headerLines[] = header.split("\\r?\\n");
            methodLine = headerLines.length > 0 ? headerLines[0] : null;
        }
        if (hasText(methodLine)) {
            String methodLineSplitted[] = methodLine.split(" ");
            if (methodLineSplitted.length >= 3) {
                rd.setMethod( getTrimmed(methodLineSplitted[0]));
                rd.setQs(getTrimmed(methodLineSplitted[1]));
                rd.setProtocol(getTrimmed(methodLineSplitted[2]));
                String[] dr = splitString(rd.getQs(), "?");
                if (dr.length >= 2) {
                    rd.setUri(getTrimmed(dr[0]));
                    rd.setParams(getTrimmed(dr[1]));
                }
            }
        }
        return rd;
    }

    private HttpCode send(HttpCode httpCode, String htmlResponse) {
        try {
            String header = getHeader(httpCode);
            PrintStream answer = new PrintStream(out, true, Utils.UTF_8_NAME);
            answer.print(header);
            if (HttpCode.CODE_OK.equals(httpCode)) {
                answer.print(htmlResponse);
            }
        } catch (IOException ex) {
            logger.log(Level.WARNING,"Problem to write to client socket");
        }
        return httpCode;
    }

    private String getHeader(HttpCode httpCode) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("HTTP/1.1 " + httpCode.getCode() + " " + httpCode.getMessage() + "\n");
        buffer.append("Date: " + new Date().toGMTString() + "\n");
        buffer.append("Accept-Ranges: none\n");
        buffer.append("\n");
        return buffer.toString();
    }

    /**
     * split string by 2 pieces by 1st separator occurs
     * @param pattern
     * @param separator
     * @return
     */
    private static String[] splitString(String pattern, String separator) {
        String result[]= new String[2];
        if (pattern != null) {
            int pos = -1;
            if (separator!= null && separator.length() > 0) {
                pos=pattern.indexOf(separator);
            }
            if (pos != -1) {
                result[0]=pattern.substring(0,pos);
                result[1]=pattern.substring(pos+separator.length());
            } else {
                result[0]=pattern;
            }
        }
        return result;
    };
    private static String getTrimmed(String string) {
        return Utils.getTrimmed(string);
    }
    private static boolean hasText(String string) {
        return Utils.hasText(string);
    }

}