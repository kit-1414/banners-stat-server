package com.kit1414.banners.server.services.httpServer;

/**
 * HTTP server API
 *
 * Created by andrey.amelin on 21-Dec-15.
 */
public interface IHttpServer {
    boolean stopServerRequest();
    void runServer();
}
