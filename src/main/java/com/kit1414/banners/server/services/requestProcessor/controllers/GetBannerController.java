package com.kit1414.banners.server.services.requestProcessor.controllers;

import com.kit1414.banners.server.services.httpServer.HttpCode;
import com.kit1414.banners.server.services.httpServer.RequestData;
import com.kit1414.banners.server.services.httpServer.ResponseData;
import com.kit1414.banners.server.services.bannersService.IBannerService;
import com.kit1414.banners.server.services.requestProcessor.IRequestController;
import com.kit1414.banners.server.services.statisticService.IStatisticsService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The GetBannerController provides action for banner body request.
 *
 * Created by kit_1414 on 24-Dec-15.
 */
public class GetBannerController extends ControllerBase implements IRequestController {
    private static final Logger logger = Logger.getLogger(GetBannerController.class.getName());
    private static final ResponseData BANNER_NOT_FOUND = new ResponseData(HttpCode.ERR_404, null);

    private IBannerService bannerService;
    private IStatisticsService statisticsService;

    private String URL_START = URL_BANNERS_REQUEST;

    public GetBannerController(IBannerService bannerService, IStatisticsService statService) {
        this.statisticsService = statService;
        this.bannerService = bannerService;
    }

    @Override
    public ResponseData processRequest(RequestData rd) {
        String bannerName = rd.getUri().substring(URL_START.length());
        logger.log(Level.INFO, "bannerName="+bannerName );
        ResponseData response = BANNER_NOT_FOUND;
        String bannerBody = bannerService.getBannerBody(bannerName);
        if (bannerBody != null) {
            logger.log(Level.INFO, "bannerBody ok");
            response = new ResponseData(HttpCode.CODE_OK, bannerBody);
            statisticsService.processBannerRequest(bannerName);
        }
        return response;
    }
    @Override
    public boolean isRequestMatch(RequestData rd) {
        return rd.getUri()!= null && rd.getUri().toLowerCase().startsWith(URL_START);
    }
}
