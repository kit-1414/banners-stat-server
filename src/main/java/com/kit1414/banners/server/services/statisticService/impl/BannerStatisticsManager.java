package com.kit1414.banners.server.services.statisticService.impl;

import com.kit1414.banners.server.services.statisticService.TopBanner;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;

/**
 * Class provides requests statistics management for one banner.
 *
 * Created by kit_1414 on 27-Dec-15.
 */
public class BannerStatisticsManager {
    /** banner ID */
    private final String bannerId;

    /** time interval to store banner requests number */
    private final long quantum;

    /** sequence of time intervals with banner requests number inside */
    private final LinkedList<BannerQuantumInfo> statistics  = new LinkedList<>();


    public BannerStatisticsManager(String bannerId, long quantum) {
        this.bannerId = bannerId;
        this.quantum = quantum;
    }

    /**
     * add info about banner request
     *
     * @param requestTime
     */
    public void processBannerRequest(long requestTime) {
        BannerQuantumInfo q = null;
        // check : if 1st quantum valid for now then add info to it.
        if (statistics.size() > 0) {
            q = statistics.getFirst();
            if (!q.isInsideQuantum(requestTime)) {
                q = null;
            }
        }
        // if no valid quantum, create new and add to list
        if (q == null) {
            q = new BannerQuantumInfo(requestTime, quantum);
            statistics.addFirst(q);
        }
        q.addRequestsNumber(1, requestTime);
    }

    /**
     * remove expired banners info
     *
     * @param requestTime - current time
     * @param deadLineInterval - expired interval
     *
     * @return 0, if all quantum expired and no more info stores inside
     */
    public boolean clearExpiredData(long requestTime, long deadLineInterval) {
        long deadLineMoment = requestTime - deadLineInterval;
        while (statistics.size() > 0 && statistics.getLast().isExpiredForTime(deadLineMoment)) {
            statistics.removeLast();
        }
        int size = statistics.size();
        return size == 0;
    }

    /**
     * Calculates requests number for defined time interval
     *
     * @param nowTime
     * @param backIntervalInMs
     * @return
     */
    public TopBanner getRequestsNumberForInterval(long nowTime, long backIntervalInMs) {
        int counter = 0;
        Long lastRequestTime = null;
        TopBanner result = null;
        lastRequestTime = getLastRequestTime();
        for (BannerQuantumInfo q : statistics) {
        long startTime = nowTime - backIntervalInMs;
           // see first quantum, until inside requested interval
           if (q.isIntersect(startTime,backIntervalInMs)) {
                counter +=q.getRequestsNumber();
           } else {
                 break;
           }
        }
        if (counter > 0) {
            result = new TopBanner(this.bannerId,counter,lastRequestTime  == null ? null : new Date(lastRequestTime ));
        }
        return result;
    }

    /**
     * returns banner last request time
     *
     * @return time in ms POSIX
     */
    private Long getLastRequestTime() {
        if (statistics.size() > 0) {
            return statistics.getFirst().getLastRequestTime();
        }
        return null;
    }


    @Override
    public String toString() {
        return "BannerStatisticsManager{" +
                "bannerId='" + bannerId + '\'' +
                // ", statistics=" + statistics +
                '}';
    }
}

