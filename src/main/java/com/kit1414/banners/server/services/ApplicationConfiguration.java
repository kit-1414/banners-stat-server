package com.kit1414.banners.server.services;

import java.util.Properties;

/**
 * Created by kit_1414 on 24-Dec-15.
 */
public class ApplicationConfiguration {

    // default values for configuration parameters
    public static int DEF_SERVER_PORT = 8080;
    public static int DEF_MAX_CONN_NUMBER = 50;
    public static String DEF_BANNER_ROOT_FOLDER = "./target/classes/bannersFolder";

    public static long DEF_BANNERS_SERVICE_TIMER_PERIOD_IN_SEC = 10;
    public static long DEF_STATISTICS_SERVICE_TIMER_PERIOD_IN_SEC = 5; // in ms
    public static long DEF_STATISTICS_SERVICE_QUANTUM_IN_MS = 300; // in ms

    // properties keys for configuration parameters
    public static String KEY_NAME_SERVER_PORT = "serverPort";
    public static String KEY_NAME_MAX_CONN_NUMBER = "serverMaxConnectionNumber";
    public static String KEY_NAME_BANNER_ROOT_FOLDER = "rootBannerFolder";

    public static String KEY_NAME_BANNERS_SERVICE_TIMER_PERIOD_IN_SEC = "bannerServiceTimerIntervalInSec";

    public static String KEY_NAME_STATISTICS_SERVICE_TIMER_PERIOD_IN_SEC = "statisticsServiceTimerIntervalInSec";
    public static String KEY_NAME_STATISTICS_SERVICE_QUANTUM_IN_MS       = "statisticsServiceQuantumInMs";


    // configuration parameters
    private int serverPort = DEF_SERVER_PORT;
    private int maxConnNumber = DEF_MAX_CONN_NUMBER;
    private String rootBannersFolder = DEF_BANNER_ROOT_FOLDER;

    private long bannersServiceTimerIntervalInSeconds = DEF_BANNERS_SERVICE_TIMER_PERIOD_IN_SEC;
    private long statisticsServiceTimerIntervalInSeconds = DEF_STATISTICS_SERVICE_TIMER_PERIOD_IN_SEC;
    private long statisticsServiceQuantumInMs = DEF_STATISTICS_SERVICE_QUANTUM_IN_MS;


    /**
     * The method loads parameters to configuration from properties file.
     *
     * @param prop
     */
    public void loadConfig(Properties prop) {
        if (prop!= null) {
            {
                String val = prop.getProperty(KEY_NAME_BANNER_ROOT_FOLDER);
                if (val != null) {
                    setRootBannersFolder(val);
                }
            }
            {
                String val = prop.getProperty(KEY_NAME_MAX_CONN_NUMBER);
                if (val != null) {
                    setMaxConnNumber(Integer.valueOf(val));
                }
            }
            {
                String val = prop.getProperty(KEY_NAME_SERVER_PORT);
                if (val != null) {
                    setServerPort(Integer.valueOf(val));
                }
            }
            {
                String val = prop.getProperty(KEY_NAME_BANNERS_SERVICE_TIMER_PERIOD_IN_SEC);
                if (val != null) {
                    setBannersServiceTimerIntervalInSeconds(Long.valueOf(val));
                }
            }
            {
                String val = prop.getProperty(KEY_NAME_STATISTICS_SERVICE_TIMER_PERIOD_IN_SEC);
                if (val != null) {
                    setStatisticsServiceTimerIntervalInSeconds(Long.valueOf(val));
                }
            }
            {
                String val = prop.getProperty(KEY_NAME_STATISTICS_SERVICE_QUANTUM_IN_MS);
                if (val != null) {
                    setStatisticsServiceQuantumInMs(Long.valueOf(val));
                }
            }
        }
    }

    // getters and setters
    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getMaxConnNumber() {
        return maxConnNumber;
    }

    public void setMaxConnNumber(int maxConnNumber) {
        this.maxConnNumber = maxConnNumber;
    }

    public String getRootBannersFolder() {
        return rootBannersFolder;
    }

    public void setRootBannersFolder(String rootBannersFolder) {
        this.rootBannersFolder = rootBannersFolder;
    }

    public long getStatisticsServiceTimerIntervalInSeconds() {
        return statisticsServiceTimerIntervalInSeconds;
    }

    public void setStatisticsServiceTimerIntervalInSeconds(long statisticsServiceTimerIntervalInSeconds) {
        this.statisticsServiceTimerIntervalInSeconds = statisticsServiceTimerIntervalInSeconds;
    }

    public long getBannersServiceTimerIntervalInSeconds() {
        return bannersServiceTimerIntervalInSeconds;
    }

    public void setBannersServiceTimerIntervalInSeconds(long bannersServiceTimerIntervalInSeconds) {
        this.bannersServiceTimerIntervalInSeconds = bannersServiceTimerIntervalInSeconds;
    }

    public long getStatisticsServiceQuantumInMs() {
        return statisticsServiceQuantumInMs;
    }

    public void setStatisticsServiceQuantumInMs(long statisticsServiceQuantumInMs) {
        this.statisticsServiceQuantumInMs = statisticsServiceQuantumInMs;
    }

    @Override
    public String toString() {
        return "ApplicationConfiguration{" +
                "serverPort=" + serverPort +
                ", maxConnNumber=" + maxConnNumber +
                ", rootBannersFolder='" + rootBannersFolder + '\'' +
                ", bannersServiceTimerIntervalInSeconds=" + bannersServiceTimerIntervalInSeconds +
                ", statisticsServiceTimerIntervalInSeconds=" + statisticsServiceTimerIntervalInSeconds +
                ", statisticsServiceQuantumInMs=" + statisticsServiceQuantumInMs +
                '}';
    }
}
