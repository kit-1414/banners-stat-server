package com.kit1414.banners.server.services.statisticService;

import com.kit1414.banners.server.utils.Utils;

import java.io.Serializable;
import java.util.Date;

/**
 * The class presents top banner info.
 *
 * Created by kit_1414 on 26-Dec-15.
 */
public class TopBanner implements Comparable, Serializable {

    /** the banner name */
    private String bannerId;

    /** banner requests number in time interval */
    private long requestNumber;

    /** last time, banner requested */
    private Date lastRequest;

    public TopBanner(String bannerId, long requestNumber, Date lastRequest) {
        this.bannerId = bannerId;
        this.requestNumber = requestNumber;
        this.lastRequest = lastRequest;
    }

    // getters and setters
    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public long getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(long requestNumber) {
        this.requestNumber = requestNumber;
    }

    public Date getLastRequest() {
        return lastRequest;
    }

    public void setLastRequest(Date lastRequest) {
        this.lastRequest = lastRequest;
    }

    public String getLastRequestStr() {
        return Utils.dateToString(lastRequest);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TopBanner that = (TopBanner) o;

        if (requestNumber != that.requestNumber) return false;
        if (bannerId != null ? !bannerId.equals(that.bannerId) : that.bannerId != null) return false;
        return !(lastRequest != null ? !lastRequest.equals(that.lastRequest) : that.lastRequest != null);

    }

    @Override
    public int hashCode() {
        int result = bannerId != null ? bannerId.hashCode() : 0;
        result = 31 * result + (int) (requestNumber ^ (requestNumber >>> 32));
        result = 31 * result + (lastRequest != null ? lastRequest.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TopBanner{" +
                "bannerId='" + bannerId + '\'' +
                ", requestNumber=" + requestNumber +
                ", lastRequest=" + lastRequest +
                '}';
    }

    /**
     * compare methods, store to sort stat results on response HTML page
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        if (o==null || !(o instanceof TopBanner)) {
            return 0;
        }
        Long i1 = new Long(getRequestNumber());
        Long i2 = new Long(((TopBanner) o).getRequestNumber());
        return i1.compareTo(i2);
    }
}

