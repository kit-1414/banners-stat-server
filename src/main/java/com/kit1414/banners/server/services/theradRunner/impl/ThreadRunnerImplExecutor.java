package com.kit1414.banners.server.services.theradRunner.impl;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ApplicationConfiguration;
import com.kit1414.banners.server.services.theradRunner.IThreadRunner;

/**
 * Created by aamelin on 31.10.2016.
 *
 * The IThreadRunner service implementation by ThreadPoolExecutor
 */
public class ThreadRunnerImplExecutor implements IThreadRunner {

    private static final Logger logger = Logger.getLogger(ThreadRunnerImplExecutor.class.getName());

    private static final int KEEP_ALIVE_TIME_SEC = 10;
    private static final int WAIT_TIME_CLIENT_THREAD_TERMINATE_SEC = KEEP_ALIVE_TIME_SEC * 2;
    private static final int INIT_CLIENT_THREADS_NUM = 5;
    private int maxConnNumber;
    private final AtomicBoolean serviceRun = new AtomicBoolean(false); // to protect from twice run and twice stop, not for run new thread on stopped service.


    private volatile ThreadPoolExecutor threadExecutor;

    public ThreadRunnerImplExecutor(ApplicationConfiguration cfg) {
        this.maxConnNumber = cfg.getMaxConnNumber();
    }

    @Override
    public void start() {
        if (serviceRun.get()) {
            return;
        }
        synchronized (serviceRun) {
            if (serviceRun.compareAndSet(false, true)) {
                this.threadExecutor = new ThreadPoolExecutor(
                        INIT_CLIENT_THREADS_NUM,
                        maxConnNumber,
                        KEEP_ALIVE_TIME_SEC/*keep alive time*/,
                        TimeUnit.SECONDS,
                        new SynchronousQueue<Runnable>()
                );
            }
        }
    }

    @Override
    public void stop() {
        if (!serviceRun.get()) {
            return;
        }
        synchronized (serviceRun) {
            if (serviceRun.compareAndSet(true, false)) {
                Thread t = new Thread(stopAllClientThreadsTask());
                t.start();
            }
        }
    }

    @Override
    public void startThread(Runnable runnable) {
        if (!serviceRun.get()) {
            return;
        }
        synchronized (serviceRun) {
            if (serviceRun.get()) {
                threadExecutor.submit(runnable);
            }
        }
    }

    private Runnable stopAllClientThreadsTask() {
        return new Runnable() {
            @Override
            public void run() {
                final String fn = "stopClientsThreads() ";
                if (threadExecutor.isTerminated()) {
                    logger.log(Level.INFO, fn + " all clients thread completed.");
                } else {
                    try {
                        logger.log(Level.INFO, fn + ">>> Attempt to shutdown thread pool ");
                        threadExecutor.shutdown();
                        // wait a N seconds for httpServer clients threads finished.
                        for (int i = 1; i <= WAIT_TIME_CLIENT_THREAD_TERMINATE_SEC; i++) {
                            boolean completedAll = threadExecutor.awaitTermination(1, TimeUnit.SECONDS);
                            logger.log(
                                    Level.INFO,
                                    fn + "all completed:" + completedAll + ",  " + i + " seconds waiting, limit for waiting is " + WAIT_TIME_CLIENT_THREAD_TERMINATE_SEC + " seconds."
                            );
                            if (completedAll) { // or if (executorService.isTerminated()) {
                                break;
                            }
                        }
                    } catch (InterruptedException e) {
                        logger.log(Level.INFO, fn + "Attempt to shutdown thread pool : tasks interrupted");
                    } finally {
                        if (!threadExecutor.isTerminated()) {
                            logger.log(Level.INFO, fn + "cancel non-finished tasks, shutdown");
                            threadExecutor.shutdownNow();
                        }
                        logger.log(Level.INFO, fn + " <<< shutdown finished");
                    }
                }
            }
        };
    }
}
