package com.kit1414.banners.server.services.statisticService.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.kit1414.banners.server.services.ApplicationConfiguration;
import com.kit1414.banners.server.services.IServiceConstants;
import com.kit1414.banners.server.services.statisticService.TopBanner;
import com.kit1414.banners.server.services.statisticService.TopBanners;
import com.kit1414.banners.server.services.statisticService.TopBannersMode;
import com.kit1414.banners.server.utils.Utils;

/**
 * The class implements detailed logic of statistics management.
 *
 * It stores requests number for any banner for some time intervals (quantum).
 * There are a sequence of such elements for any banners.
 *
 * All logic (add new stat info, top stat recalculation, expired info delete ) executes in the separated work thread.
 *
 * Such solution simplify at all any synchronization problem  (code logic and time optimization)
 *
 * All tasks pass to the work thread via blocking queue.
 *
 * Top stats calculated every <i>statRefreshInterval<i/> ms and stores as <i>topStats<i/>.
 *
 * Created by kit_1414 on 27-Dec-15.
 *
 */
public class StatisticManager {
    private static final Logger logger = Logger.getLogger(StatisticManager.class.getName());

    /** banner file expiration interval. Use in service to remove banners info, requested more this interval ago. */
    private static final long expirationInterval = TopBannersMode.TOP_100_24_H.getTimeInterval();

    /** blocking queue to pass tasks to work thread. */
    private final BlockingQueue<StatJob> taskQueue  = new LinkedBlockingQueue<>();

    /** all banners requests info */
    private final Map<String,BannerStatisticsManager> statistics = new HashMap<>();

    /** Calculated top banners info */
    private CalculatedTopBanners topStats;

    /** Sync object for top banners info */
    private final Object topStatsLocker = new Object();

    /** time interval to refresh top stats and delete expired data */
    private long statRefreshInterval;

    /** time interval to store banner requests number */
    private long quantum;

    /**
     * parameters initialization
     *
     * @param cfg
     */
    public void init(ApplicationConfiguration cfg) {
        this.statRefreshInterval = cfg.getStatisticsServiceTimerIntervalInSeconds() *1000;
        this.quantum = cfg.getStatisticsServiceQuantumInMs();
        // create empty stat map
        long nowTime = System.currentTimeMillis();

        setTopStats( new CalculatedTopBanners( nowTime,nowTime + statRefreshInterval));
    }

    /**
     * Add task to update banner request info
     *
     * @param bannerId
     * @param start - event time in ms
     */
    public boolean processBannerRequest(String bannerId, long start) {
        // element add ok (return true), if there is free space. Or return else
        return taskQueue.offer(new ProcessBanner(bannerId,start,quantum, this));
    }

    /**
     * stop service  - ask to stop work thread.
     */
    public void stopService() {
        taskQueue.add(new StopStatThread());
    }

    /**
     * add task to  recalculate and update top banners stats
     *
     * @param nowTime
     */
    public void recalculateTopStat(long nowTime) {
        taskQueue.add(new RefreshTopBanners(nowTime, this));
    }

    /**
     * Update banner requests number info .
     *
     * @param bannerId -
     * @param start - event time in ms
     * @param quantum - time interval to store banners requests in (ms)
     */
    private void processBannerTask(String bannerId, long start, long quantum) {
        BannerStatisticsManager bm;
        bm = statistics.get(bannerId);
        if (bm == null) {
            bm = new BannerStatisticsManager(bannerId,quantum);
            statistics.put(bannerId, bm);
        }
        bm.processBannerRequest(start);
    }

    /**
     * Removes expired banners info
     *
     * @param start
     */
    private void clearExpiredDataTask(long start) {
        List<String> bannersToDelete = new ArrayList<>();
        for (Map.Entry<String,BannerStatisticsManager> e : statistics.entrySet()) {
            BannerStatisticsManager bm = e.getValue();
            if (bm.clearExpiredData(start, expirationInterval)) {
                bannersToDelete.add(e.getKey());
            }
        }
        if (bannersToDelete.size() > 0) {
            for (String key : bannersToDelete) {
                statistics.remove(key);
            }
        }
    }

    /**
     * Calculates top banners for the time interval.
     *
     * @param nowTime
     * @param backIntervalInMs
     * @return
     */
    private TopBanner[] getTopForInterval(long nowTime, long backIntervalInMs) {
        PriorityQueue<TopBanner> pq  = new PriorityQueue<>();
        for (BannerStatisticsManager mgr : statistics.values()) {
            TopBanner sb = mgr.getRequestsNumberForInterval(nowTime, backIntervalInMs);
            if (sb != null) {
                pq.add(sb);
            }
            // remove elements with low requests number, if collection size is more limit.
            while (pq.size() > IServiceConstants.topBannerNumber) {
                pq.remove();
            }
        }
        ArrayList<TopBanner> list = new ArrayList<>(pq);
        Collections.sort(list); // ASC
        Collections.reverse(list); // to DESC

        TopBanner[] resultArray = list.toArray(new TopBanner[list.size()]);
        return resultArray;
    }

    /**
     * Recalculates top banners for all intervals and update calculated top banners info.
     * @param nowTime
     */
    private void recalculateTopStatTask(long nowTime) {
        Map<TopBannersMode, TopBanner[]> map = new HashMap<>();
        for (TopBannersMode e : TopBannersMode.values()) {
            TopBanner[] res = getTopForInterval(nowTime,e.getTimeInterval());
            map.put(e, res);
        }
        CalculatedTopBanners topBeans = new CalculatedTopBanners(nowTime, nowTime + statRefreshInterval, map);
        setTopStats(topBeans);
    }

    /**
     * Sets calculated top banners info
     *
     * @param topStats
     */
    public void setTopStats(CalculatedTopBanners topStats) {
        synchronized (topStatsLocker) {
            this.topStats = topStats;
        }
    }

    /**
     * Gets calculated top banners info
     *
     * @param e - top banners interval
     *
     * @return top banners info
     */
    public TopBanners getTopStatByMode(TopBannersMode e) {
        TopBanners result = null;
        synchronized (topStatsLocker) {
            CalculatedTopBanners data = topStats;
            if (data!= null) {
                result = data.getTopStatResultBean(e);
            }
        }
        return result;
    }

    /**
     * Work thread to update banners info, recalculate top banners and stop thread.
     *
     */
    public void startTasksWaitingThread() {
        Thread t =  new Thread(new Runnable() {
            @Override
            public void run() {
                boolean stopJob = false;
                logger.log(Level.INFO, " loop started ");

                while(!stopJob || Thread.interrupted()) {
                    try {
                        StatJob task = taskQueue.take(); // blocking call
                        logger.log(Level.INFO, "Job received : "+task);
                        stopJob = task.doJob();
                    } catch (Exception ex) {
                        // we don't want to stop thread by any exception, just by certain command
                        logger.log(Level.WARNING, "Unexpected exception happen : " + ex, ex);
                    }
                }
                logger.log(Level.INFO, " loop finished. ");
            }
        });
        // t.setDaemon(true);
        t.start();
    }

    /**
     * Base class for work thread tack
     */
    static abstract class StatJob {
        abstract boolean doJob();
    };

    /**
     * Work thread task to stop work thread
     *
     */
    static class StopStatThread extends StatJob {
        @Override
        public boolean doJob() {
            return true; // Stop server task
        }
        @Override
        public String toString() {
            return "StopStatThread{}";
        }
    };

    /**
     * Work thread task to refresh top banners and clear expired data
     *
     */
    private static class RefreshTopBanners extends StatJob {
        private final long startTime;
        private final StatisticManager manager;


        RefreshTopBanners(long startTime, StatisticManager manager) {
            this.startTime = startTime;
            this.manager = manager;
        }

        @Override
        public boolean doJob() {
            manager.recalculateTopStatTask(startTime);
            manager.clearExpiredDataTask(startTime);
            logger.log(Level.INFO, "Request to refresh top stats done");
            return false;
        }

        @Override
        public String toString() {
            return "RefreshTopBanners{" +
                    "startTime=" + startTime +
                    '}';
        }
    };

    /**
     * Work thread task to update banner requests number info.
     *
     */

    private static class ProcessBanner extends StatJob {
        private final String bannerId;
        private final long eventTime;
        private final long quantum;
        private final StatisticManager manager;

        ProcessBanner(String bannerId, long eventTime, long quantum, StatisticManager manager) {
            this.bannerId = bannerId;
            this.eventTime = eventTime;
            this.quantum = quantum;
            this.manager = manager;
        }
        @Override
        public boolean doJob() {
            manager.processBannerTask(bannerId, eventTime, quantum);
            logger.log(Level.INFO, "Request to process banner done : " + this);
            return false;
        }

        @Override
        public String toString() {
            return "ProcessBanner{" +
                    " bannerId='" + bannerId + '\'' +
                    ", eventTime=" + eventTime +
                    ", quantum=" + quantum +
                    '}';
        }
    };
}
