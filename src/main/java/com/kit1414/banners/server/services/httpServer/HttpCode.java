package com.kit1414.banners.server.services.httpServer;

/**
 * Created by kit_1414 on 26-Dec-15.
 */
public enum HttpCode {

    CODE_OK(200, "OK"),
    ERR_403(403, "Forbidden"),
    ERR_404(404, "Not found"),
    ERR_500(500, "Internal Server Error");

    private final int code;
    private final String message;

    HttpCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
