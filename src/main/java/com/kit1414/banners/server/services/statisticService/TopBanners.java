package com.kit1414.banners.server.services.statisticService;

import java.io.Serializable;

/**
 * Created by kit_1414 on 03-Jan-16.
 *
 * Class presents top banners info for defined time interval.
 *
 * This class can be read from many threads (clients requests) at same time.
 *
 */
public class TopBanners implements Serializable {

    /** stat time calculated at */
    private final long statCalTime;

    /** next moment stat time calculation */
    private final long nextCalcTime;

    /** top banners stat info*/
    private final TopBanner[] topBanners;

    // The constructor
    public TopBanners(long statCalTime, long nextCalcTime, TopBanner[] topBanners) {
        this.statCalTime = statCalTime;
        this.nextCalcTime = nextCalcTime;
        this.topBanners = topBanners;
    }

    // getters
    public long getStatCalTime() {
        return statCalTime;
    }

    public long getNextCalcTime() {
        return nextCalcTime;
    }

    public TopBanner[] getTopBanners() {
        return topBanners;
    }
}
