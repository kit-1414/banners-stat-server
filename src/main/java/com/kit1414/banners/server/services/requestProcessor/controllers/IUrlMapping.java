package com.kit1414.banners.server.services.requestProcessor.controllers;

/**
 * Created by kit_1414 on 24-Dec-15.
 */
public interface IUrlMapping {
    // HTTP url pattern to requests banner body
    String URL_BANNERS_REQUEST = "/banner/";

    // HTTP url pattern to stop application
    String URL_STOP_SERVER     = "/stopserver";

    // HTTP url patterns to request a top banners for intervals
    String URL_STAT_TOP_24_H   = "/stat/top_100_24_h";
    String URL_STAT_TOP_4_H    = "/stat/top_100_4_h";
    String URL_STAT_TOP_1_H    = "/stat/top_100_1_h";
    String URL_STAT_TOP_1_MIN  = "/stat/top_100_1_min";
}
