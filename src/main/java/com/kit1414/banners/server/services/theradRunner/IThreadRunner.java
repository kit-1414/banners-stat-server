package com.kit1414.banners.server.services.theradRunner;

/**
 * Created by aamelin on 31.10.2016.
 *
 * Service to run threads for HTTP client connections.
 *
 */
public interface IThreadRunner {
    /**
     *   Start service
     */
    void start();
    /**
     * Stop service
     */

    void stop();

    /**
     * Start new Thread
     * @param runnable
     */
    void startThread(Runnable runnable);
}
