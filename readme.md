
Task definition.
================

Its required to write an application that will give the banner code.
The application is a http-server that when you call the GET / banner / {id} gives the body (html code) of the banner.
Banners are presented as files named {id} .html, stored in a known directory.
Banners older than two weeks are expired. Http error 404 must be return for such banner request.

A task:
• http-server return banner body when requesting by "/banner/{id}"
• must be able to upload new banners without restarting the application
• banners that were loaded 2 weeks ago or earlier, should not be given and can be removed
• if the banner is outdated or not found, you must return Http error 404
• It is necessary to keep statistics on the banners in the past 24 hours, in the last 4 hours, the last hour and the last minute:
top 100 banner requests count with the format: banner id, requests count, last access time.
• the problem should be solved in Java 7 SE, without the use of third-party libraries


Usage:
======

 - compile code by maven: mvn clean package (test banner files must be copy to target folder to work app OK by def cfg)
 - Start the server: ./runServer.cmd
   the file properties name  passes as a parameter
 - Open test page ./testPage.html
 - Enjoy.

Implementation:
===============

ServerApplicationRunner  - main class to launch the application.
Configuration file name passes to the application as 1st argument.


Application implemented by services -

 - ServiceFactory: provides services life cycle logic: creation, initialization, binding, destroy.

 - IThreadRunner: provides new thread run for HTTP client connections.

 - IHttpServer presents HTTP Web server implementation : it processes http requests.

 - IRequestProcessor : Http request handler -  it passes http requests to specific controllers

 - IBannerService  - processes banners body requests - looking at the files, if file exists and it doesn't expired - returns body else return 404 Error
   There is removal expired files feature. It runs by timer.
   New banners loading : just put banner file to the right folder.

 - IStatisticsService - implements banner requests number statistics storage and top banners calculations.
   Service calls to increase the banner requests number if banner body HTTP request returns ok.
   There is removal expired data feature. It calls by timer.
   The top 100 banners info calculates by timer task.

   Whole service logic : add banners requests info, top banners calculation, expired info removing execute
   in separated work thread. Job requests pass to work thread by blocking queue.

   Banners requests number statistics store by requests number inside time intervals (quantum).
   BTW banner requests info presents as list of such intervals (quantum).

   Chain of 1 or more quantum with requests sum in the interval stores for each banner
   (if it has been requested 24 hours ago or earlier)
   This solution allows to process a huge banners requests number without memory usage increasing.

